import 'package:freezed_annotation/freezed_annotation.dart';
import 'package:flutter/foundation.dart';

part 'appuser.freezed.dart';
part 'appuser.g.dart';

@freezed
class AppUser with _$AppUser {
  const factory AppUser({
    @JsonKey(name: 'email', defaultValue: '') String? email,
    @JsonKey(name: 'password', defaultValue: '') String? password,
    @JsonKey(name: 'id', defaultValue: '') String? id,
  }) = _AppUser;

  factory AppUser.fromJson(Map<String, dynamic> json) =>
      _$AppUserFromJson(json);

  factory AppUser.initial() => AppUser();
}
