// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'appuser.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

_$_AppUser _$$_AppUserFromJson(Map<String, dynamic> json) => _$_AppUser(
      email: json['email'] as String? ?? '',
      password: json['password'] as String? ?? '',
      id: json['id'] as String? ?? '',
    );

Map<String, dynamic> _$$_AppUserToJson(_$_AppUser instance) =>
    <String, dynamic>{
      'email': instance.email,
      'password': instance.password,
      'id': instance.id,
    };
