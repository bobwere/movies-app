import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:movies/models/user/appuser.dart';
import 'package:movies/services/auth/auth_state/auth_state.dart';

class AuthNotifier extends StateNotifier<AsyncValue<AuthState>> {
  AuthNotifier() : super(AsyncValue.data(AuthState.initial()));

  Future<void> login({required String email, required String password}) async {
    try {
      state = AsyncValue.data(
          state.value!.copyWith.call(failureLogin: '', isLoadingLogin: true));
      // Once signed in, return the UserCredential
      final UserCredential _userCredentials =
          await FirebaseAuth.instance.signInWithEmailAndPassword(
        email: email,
        password: password,
      );

      //get firebase user class
      final User? user = _userCredentials.user;

      //get user collection
      final CollectionReference<Map<String, dynamic>> usersRef =
          FirebaseFirestore.instance.collection('users');

      //get user info  stored in the firebase user collection
      final DocumentSnapshot<Map<String, dynamic>> document =
          await usersRef.doc(user?.uid).get();

      final AppUser appUser = AppUser.fromJson(document.data()!);

      state = AsyncValue.data(state.value!.copyWith.call(
        failureLogin: '',
        isLoadingLogin: false,
        appUser: appUser,
      ));
    } on FirebaseAuthException catch (e) {
      state = AsyncValue.data(state.value!.copyWith
          .call(failureLogin: e.message, isLoadingLogin: false));
    }
  }

  Future<void> createAccount(
      {required String email, required String password}) async {
    try {
      state = AsyncValue.data(state.value!.copyWith
          .call(failureLogin: '', isLoadingCreateAccount: true));
      // Once signed in, return the UserCredential
      final UserCredential _userCredentials =
          await FirebaseAuth.instance.createUserWithEmailAndPassword(
        email: email,
        password: password,
      );

      //get firebase user class
      final User? user = _userCredentials.user;

      //transfer firebase user data  to our internal class
      final AppUser _userBioData = AppUser(
        id: user?.uid,
        email: user?.email,
      );

      //get user collection
      final CollectionReference<Map<String, dynamic>> usersRef =
          FirebaseFirestore.instance.collection('users');

      if (_userCredentials.additionalUserInfo?.isNewUser ?? false) {
        //if user is new store data to user collection
        await usersRef.doc(user?.uid).set(_userBioData.toJson());
      }

      state = AsyncValue.data(state.value!.copyWith.call(
        failureCreateAccount: '',
        isLoadingCreateAccount: false,
        appUser: _userBioData,
      ));
    } on FirebaseAuthException catch (e) {
      state = AsyncValue.data(
        state.value!.copyWith.call(
            failureCreateAccount: e.message, isLoadingCreateAccount: false),
      );
    }
  }

  Future<void> resetPasword({required String email}) async {
    try {
      state = AsyncValue.data(
        state.value!.copyWith.call(
          failureResetPassword: '',
          isLoadingResetPassword: true,
          hasSentResetLink: false,
        ),
      );
      //send password reset email
      await FirebaseAuth.instance.sendPasswordResetEmail(email: email);

      state = AsyncValue.data(
        state.value!.copyWith.call(
          failureResetPassword: '',
          isLoadingResetPassword: false,
          hasSentResetLink: true,
        ),
      );
    } on FirebaseAuthException catch (e) {
      state = AsyncValue.data(
        state.value!.copyWith.call(
            failureResetPassword: e.message, isLoadingResetPassword: false),
      );
    }
  }

  Future<void> logOut() async {
    await FirebaseAuth.instance.signOut();
    state = AsyncValue.data(
      state.value!.copyWith.call(
        appUser: null,
      ),
    );
  }
}

final authProvider = StateNotifierProvider<AuthNotifier, AsyncValue<AuthState>>(
    (ref) => AuthNotifier());
