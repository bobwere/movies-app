import 'package:freezed_annotation/freezed_annotation.dart';
import 'package:movies/models/user/appuser.dart';

part 'auth_state.freezed.dart';

@freezed
class AuthState with _$AuthState {
  const factory AuthState({
    //
    bool? isLoadingLogin,
    bool? isLoadingCreateAccount,
    bool? isLoadingResetPassword,
    //
    String? failureLogin,
    String? failureResetPassword,
    String? failureCreateAccount,
    //
    String? email,
    String? password,
    //
    bool? emailIsValid,
    bool? passwordIsValid,
    //
    bool? hasAttemptedFirstSubmit,
    //
    AppUser? appUser,
    //
    bool? hasSentResetLink,
  }) = _AuthState;

  factory AuthState.initial() => AuthState(
        email: '',
        emailIsValid: false,
        hasSentResetLink: false,
        failureCreateAccount: '',
        failureLogin: '',
        failureResetPassword: '',
        hasAttemptedFirstSubmit: false,
        isLoadingCreateAccount: false,
        isLoadingLogin: false,
        isLoadingResetPassword: false,
        password: '',
        passwordIsValid: false,
        appUser: null,
      );
}
