// coverage:ignore-file
// GENERATED CODE - DO NOT MODIFY BY HAND
// ignore_for_file: type=lint
// ignore_for_file: unused_element, deprecated_member_use, deprecated_member_use_from_same_package, use_function_type_syntax_for_parameters, unnecessary_const, avoid_init_to_null, invalid_override_different_default_values_named, prefer_expression_function_bodies, annotate_overrides, invalid_annotation_target

part of 'movie.dart';

// **************************************************************************
// FreezedGenerator
// **************************************************************************

T _$identity<T>(T value) => value;

final _privateConstructorUsedError = UnsupportedError(
    'It seems like you constructed your class using `MyClass._()`. This constructor is only meant to be used by freezed and you are not supposed to need it nor use it.\nPlease check the documentation here for more information: https://github.com/rrousselGit/freezed#custom-getters-and-methods');

Movie _$MovieFromJson(Map<String, dynamic> json) {
  return _Movie.fromJson(json);
}

/// @nodoc
mixin _$Movie {
  @JsonKey(name: 'Title', defaultValue: '')
  String? get title => throw _privateConstructorUsedError;
  @JsonKey(name: 'Year', defaultValue: '')
  String? get year => throw _privateConstructorUsedError;
  @JsonKey(name: 'Poster', defaultValue: '')
  String? get poster => throw _privateConstructorUsedError;
  @JsonKey(name: 'imdbID', defaultValue: '')
  String? get id => throw _privateConstructorUsedError;
  @JsonKey(name: 'Type', defaultValue: '')
  String? get type => throw _privateConstructorUsedError;

  Map<String, dynamic> toJson() => throw _privateConstructorUsedError;
  @JsonKey(ignore: true)
  $MovieCopyWith<Movie> get copyWith => throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $MovieCopyWith<$Res> {
  factory $MovieCopyWith(Movie value, $Res Function(Movie) then) =
      _$MovieCopyWithImpl<$Res>;
  $Res call(
      {@JsonKey(name: 'Title', defaultValue: '') String? title,
      @JsonKey(name: 'Year', defaultValue: '') String? year,
      @JsonKey(name: 'Poster', defaultValue: '') String? poster,
      @JsonKey(name: 'imdbID', defaultValue: '') String? id,
      @JsonKey(name: 'Type', defaultValue: '') String? type});
}

/// @nodoc
class _$MovieCopyWithImpl<$Res> implements $MovieCopyWith<$Res> {
  _$MovieCopyWithImpl(this._value, this._then);

  final Movie _value;
  // ignore: unused_field
  final $Res Function(Movie) _then;

  @override
  $Res call({
    Object? title = freezed,
    Object? year = freezed,
    Object? poster = freezed,
    Object? id = freezed,
    Object? type = freezed,
  }) {
    return _then(_value.copyWith(
      title: title == freezed
          ? _value.title
          : title // ignore: cast_nullable_to_non_nullable
              as String?,
      year: year == freezed
          ? _value.year
          : year // ignore: cast_nullable_to_non_nullable
              as String?,
      poster: poster == freezed
          ? _value.poster
          : poster // ignore: cast_nullable_to_non_nullable
              as String?,
      id: id == freezed
          ? _value.id
          : id // ignore: cast_nullable_to_non_nullable
              as String?,
      type: type == freezed
          ? _value.type
          : type // ignore: cast_nullable_to_non_nullable
              as String?,
    ));
  }
}

/// @nodoc
abstract class _$MovieCopyWith<$Res> implements $MovieCopyWith<$Res> {
  factory _$MovieCopyWith(_Movie value, $Res Function(_Movie) then) =
      __$MovieCopyWithImpl<$Res>;
  @override
  $Res call(
      {@JsonKey(name: 'Title', defaultValue: '') String? title,
      @JsonKey(name: 'Year', defaultValue: '') String? year,
      @JsonKey(name: 'Poster', defaultValue: '') String? poster,
      @JsonKey(name: 'imdbID', defaultValue: '') String? id,
      @JsonKey(name: 'Type', defaultValue: '') String? type});
}

/// @nodoc
class __$MovieCopyWithImpl<$Res> extends _$MovieCopyWithImpl<$Res>
    implements _$MovieCopyWith<$Res> {
  __$MovieCopyWithImpl(_Movie _value, $Res Function(_Movie) _then)
      : super(_value, (v) => _then(v as _Movie));

  @override
  _Movie get _value => super._value as _Movie;

  @override
  $Res call({
    Object? title = freezed,
    Object? year = freezed,
    Object? poster = freezed,
    Object? id = freezed,
    Object? type = freezed,
  }) {
    return _then(_Movie(
      title: title == freezed
          ? _value.title
          : title // ignore: cast_nullable_to_non_nullable
              as String?,
      year: year == freezed
          ? _value.year
          : year // ignore: cast_nullable_to_non_nullable
              as String?,
      poster: poster == freezed
          ? _value.poster
          : poster // ignore: cast_nullable_to_non_nullable
              as String?,
      id: id == freezed
          ? _value.id
          : id // ignore: cast_nullable_to_non_nullable
              as String?,
      type: type == freezed
          ? _value.type
          : type // ignore: cast_nullable_to_non_nullable
              as String?,
    ));
  }
}

/// @nodoc
@JsonSerializable()
class _$_Movie with DiagnosticableTreeMixin implements _Movie {
  const _$_Movie(
      {@JsonKey(name: 'Title', defaultValue: '') this.title,
      @JsonKey(name: 'Year', defaultValue: '') this.year,
      @JsonKey(name: 'Poster', defaultValue: '') this.poster,
      @JsonKey(name: 'imdbID', defaultValue: '') this.id,
      @JsonKey(name: 'Type', defaultValue: '') this.type});

  factory _$_Movie.fromJson(Map<String, dynamic> json) =>
      _$$_MovieFromJson(json);

  @override
  @JsonKey(name: 'Title', defaultValue: '')
  final String? title;
  @override
  @JsonKey(name: 'Year', defaultValue: '')
  final String? year;
  @override
  @JsonKey(name: 'Poster', defaultValue: '')
  final String? poster;
  @override
  @JsonKey(name: 'imdbID', defaultValue: '')
  final String? id;
  @override
  @JsonKey(name: 'Type', defaultValue: '')
  final String? type;

  @override
  String toString({DiagnosticLevel minLevel = DiagnosticLevel.info}) {
    return 'Movie(title: $title, year: $year, poster: $poster, id: $id, type: $type)';
  }

  @override
  void debugFillProperties(DiagnosticPropertiesBuilder properties) {
    super.debugFillProperties(properties);
    properties
      ..add(DiagnosticsProperty('type', 'Movie'))
      ..add(DiagnosticsProperty('title', title))
      ..add(DiagnosticsProperty('year', year))
      ..add(DiagnosticsProperty('poster', poster))
      ..add(DiagnosticsProperty('id', id))
      ..add(DiagnosticsProperty('type', type));
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _Movie &&
            const DeepCollectionEquality().equals(other.title, title) &&
            const DeepCollectionEquality().equals(other.year, year) &&
            const DeepCollectionEquality().equals(other.poster, poster) &&
            const DeepCollectionEquality().equals(other.id, id) &&
            const DeepCollectionEquality().equals(other.type, type));
  }

  @JsonKey(ignore: true)
  @override
  int get hashCode => Object.hash(
      runtimeType,
      const DeepCollectionEquality().hash(title),
      const DeepCollectionEquality().hash(year),
      const DeepCollectionEquality().hash(poster),
      const DeepCollectionEquality().hash(id),
      const DeepCollectionEquality().hash(type));

  @JsonKey(ignore: true)
  @override
  _$MovieCopyWith<_Movie> get copyWith =>
      __$MovieCopyWithImpl<_Movie>(this, _$identity);

  @override
  Map<String, dynamic> toJson() {
    return _$$_MovieToJson(this);
  }
}

abstract class _Movie implements Movie {
  const factory _Movie(
      {@JsonKey(name: 'Title', defaultValue: '') final String? title,
      @JsonKey(name: 'Year', defaultValue: '') final String? year,
      @JsonKey(name: 'Poster', defaultValue: '') final String? poster,
      @JsonKey(name: 'imdbID', defaultValue: '') final String? id,
      @JsonKey(name: 'Type', defaultValue: '') final String? type}) = _$_Movie;

  factory _Movie.fromJson(Map<String, dynamic> json) = _$_Movie.fromJson;

  @override
  @JsonKey(name: 'Title', defaultValue: '')
  String? get title => throw _privateConstructorUsedError;
  @override
  @JsonKey(name: 'Year', defaultValue: '')
  String? get year => throw _privateConstructorUsedError;
  @override
  @JsonKey(name: 'Poster', defaultValue: '')
  String? get poster => throw _privateConstructorUsedError;
  @override
  @JsonKey(name: 'imdbID', defaultValue: '')
  String? get id => throw _privateConstructorUsedError;
  @override
  @JsonKey(name: 'Type', defaultValue: '')
  String? get type => throw _privateConstructorUsedError;
  @override
  @JsonKey(ignore: true)
  _$MovieCopyWith<_Movie> get copyWith => throw _privateConstructorUsedError;
}
