import 'package:flutter/material.dart';
import 'package:flutter/scheduler.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:movies/models/user/appuser.dart';
import 'package:movies/presentation/auth/login/login_widget.dart';
import 'package:movies/presentation/home/main/main_page.dart';
import 'package:movies/services/auth/auth_provider/auth_provider.dart';
import 'package:movies/services/auth/auth_state/auth_state.dart';

class SplashPage extends ConsumerStatefulWidget {
  const SplashPage({Key? key}) : super(key: key);

  @override
  _SplashPageState createState() => _SplashPageState();
}

class _SplashPageState extends ConsumerState<SplashPage> {
  @override
  void initState() {
    SchedulerBinding.instance?.addPostFrameCallback((_) {
      navigateToHomePage();
    });
    super.initState();
  }

  Future<void> navigateToHomePage() async {
    Future<void>.delayed(
      const Duration(seconds: 2),
      () {
        AppUser? appUser = ref.read(authProvider).value?.appUser ?? null;

        if (appUser != null) {
          Navigator.pop(context);
          Navigator.of(context).push(
            MaterialPageRoute(
              builder: (context) => MainPage(),
            ),
          );
        } else {
          Navigator.pop(context);
          Navigator.of(context).push(
            MaterialPageRoute(
              builder: (context) => LoginWidget(),
            ),
          );
        }
      },
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Center(
        child: Text(
          'Movies',
          style: Theme.of(context).textTheme.headline5,
        ),
      ),
    );
  }
}
