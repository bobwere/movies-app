import 'package:flutter/material.dart';
import 'package:movies/presentation/auth/create_account/create_account.dart';
import 'package:movies/presentation/home/main/main_page.dart';
import 'package:movies/presentation/splash/splash_page.dart';

class App extends StatelessWidget {
  const App({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Movie App',
      useInheritedMediaQuery: true,
      theme: ThemeData.light()
          .copyWith(primaryColor: Color.fromARGB(255, 53, 16, 114)),
      debugShowCheckedModeBanner: false,
      home: SplashPage(),
    );
  }
}
