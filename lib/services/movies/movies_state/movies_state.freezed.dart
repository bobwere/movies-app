// coverage:ignore-file
// GENERATED CODE - DO NOT MODIFY BY HAND
// ignore_for_file: type=lint
// ignore_for_file: unused_element, deprecated_member_use, deprecated_member_use_from_same_package, use_function_type_syntax_for_parameters, unnecessary_const, avoid_init_to_null, invalid_override_different_default_values_named, prefer_expression_function_bodies, annotate_overrides, invalid_annotation_target

part of 'movies_state.dart';

// **************************************************************************
// FreezedGenerator
// **************************************************************************

T _$identity<T>(T value) => value;

final _privateConstructorUsedError = UnsupportedError(
    'It seems like you constructed your class using `MyClass._()`. This constructor is only meant to be used by freezed and you are not supposed to need it nor use it.\nPlease check the documentation here for more information: https://github.com/rrousselGit/freezed#custom-getters-and-methods');

/// @nodoc
mixin _$MoviesState {
  bool? get isLoadingMovies => throw _privateConstructorUsedError;
  String? get failureGettingMovies => throw _privateConstructorUsedError;
  List<Movie>? get movies => throw _privateConstructorUsedError; //
  bool? get isLoadingFavouritesMovies => throw _privateConstructorUsedError;
  String? get failureGettingFavouritesMovies =>
      throw _privateConstructorUsedError;
  List<Movie>? get favouriteMovies => throw _privateConstructorUsedError; //
  bool? get isLoadingAddFavouritesMovies => throw _privateConstructorUsedError;
  String? get failureAddingFavouritesMovies =>
      throw _privateConstructorUsedError; //
  bool? get isLoadingRemoveFavouritesMovies =>
      throw _privateConstructorUsedError;
  String? get failureRemovingFavouritesMovies =>
      throw _privateConstructorUsedError;

  @JsonKey(ignore: true)
  $MoviesStateCopyWith<MoviesState> get copyWith =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $MoviesStateCopyWith<$Res> {
  factory $MoviesStateCopyWith(
          MoviesState value, $Res Function(MoviesState) then) =
      _$MoviesStateCopyWithImpl<$Res>;
  $Res call(
      {bool? isLoadingMovies,
      String? failureGettingMovies,
      List<Movie>? movies,
      bool? isLoadingFavouritesMovies,
      String? failureGettingFavouritesMovies,
      List<Movie>? favouriteMovies,
      bool? isLoadingAddFavouritesMovies,
      String? failureAddingFavouritesMovies,
      bool? isLoadingRemoveFavouritesMovies,
      String? failureRemovingFavouritesMovies});
}

/// @nodoc
class _$MoviesStateCopyWithImpl<$Res> implements $MoviesStateCopyWith<$Res> {
  _$MoviesStateCopyWithImpl(this._value, this._then);

  final MoviesState _value;
  // ignore: unused_field
  final $Res Function(MoviesState) _then;

  @override
  $Res call({
    Object? isLoadingMovies = freezed,
    Object? failureGettingMovies = freezed,
    Object? movies = freezed,
    Object? isLoadingFavouritesMovies = freezed,
    Object? failureGettingFavouritesMovies = freezed,
    Object? favouriteMovies = freezed,
    Object? isLoadingAddFavouritesMovies = freezed,
    Object? failureAddingFavouritesMovies = freezed,
    Object? isLoadingRemoveFavouritesMovies = freezed,
    Object? failureRemovingFavouritesMovies = freezed,
  }) {
    return _then(_value.copyWith(
      isLoadingMovies: isLoadingMovies == freezed
          ? _value.isLoadingMovies
          : isLoadingMovies // ignore: cast_nullable_to_non_nullable
              as bool?,
      failureGettingMovies: failureGettingMovies == freezed
          ? _value.failureGettingMovies
          : failureGettingMovies // ignore: cast_nullable_to_non_nullable
              as String?,
      movies: movies == freezed
          ? _value.movies
          : movies // ignore: cast_nullable_to_non_nullable
              as List<Movie>?,
      isLoadingFavouritesMovies: isLoadingFavouritesMovies == freezed
          ? _value.isLoadingFavouritesMovies
          : isLoadingFavouritesMovies // ignore: cast_nullable_to_non_nullable
              as bool?,
      failureGettingFavouritesMovies: failureGettingFavouritesMovies == freezed
          ? _value.failureGettingFavouritesMovies
          : failureGettingFavouritesMovies // ignore: cast_nullable_to_non_nullable
              as String?,
      favouriteMovies: favouriteMovies == freezed
          ? _value.favouriteMovies
          : favouriteMovies // ignore: cast_nullable_to_non_nullable
              as List<Movie>?,
      isLoadingAddFavouritesMovies: isLoadingAddFavouritesMovies == freezed
          ? _value.isLoadingAddFavouritesMovies
          : isLoadingAddFavouritesMovies // ignore: cast_nullable_to_non_nullable
              as bool?,
      failureAddingFavouritesMovies: failureAddingFavouritesMovies == freezed
          ? _value.failureAddingFavouritesMovies
          : failureAddingFavouritesMovies // ignore: cast_nullable_to_non_nullable
              as String?,
      isLoadingRemoveFavouritesMovies: isLoadingRemoveFavouritesMovies ==
              freezed
          ? _value.isLoadingRemoveFavouritesMovies
          : isLoadingRemoveFavouritesMovies // ignore: cast_nullable_to_non_nullable
              as bool?,
      failureRemovingFavouritesMovies: failureRemovingFavouritesMovies ==
              freezed
          ? _value.failureRemovingFavouritesMovies
          : failureRemovingFavouritesMovies // ignore: cast_nullable_to_non_nullable
              as String?,
    ));
  }
}

/// @nodoc
abstract class _$MoviesStateCopyWith<$Res>
    implements $MoviesStateCopyWith<$Res> {
  factory _$MoviesStateCopyWith(
          _MoviesState value, $Res Function(_MoviesState) then) =
      __$MoviesStateCopyWithImpl<$Res>;
  @override
  $Res call(
      {bool? isLoadingMovies,
      String? failureGettingMovies,
      List<Movie>? movies,
      bool? isLoadingFavouritesMovies,
      String? failureGettingFavouritesMovies,
      List<Movie>? favouriteMovies,
      bool? isLoadingAddFavouritesMovies,
      String? failureAddingFavouritesMovies,
      bool? isLoadingRemoveFavouritesMovies,
      String? failureRemovingFavouritesMovies});
}

/// @nodoc
class __$MoviesStateCopyWithImpl<$Res> extends _$MoviesStateCopyWithImpl<$Res>
    implements _$MoviesStateCopyWith<$Res> {
  __$MoviesStateCopyWithImpl(
      _MoviesState _value, $Res Function(_MoviesState) _then)
      : super(_value, (v) => _then(v as _MoviesState));

  @override
  _MoviesState get _value => super._value as _MoviesState;

  @override
  $Res call({
    Object? isLoadingMovies = freezed,
    Object? failureGettingMovies = freezed,
    Object? movies = freezed,
    Object? isLoadingFavouritesMovies = freezed,
    Object? failureGettingFavouritesMovies = freezed,
    Object? favouriteMovies = freezed,
    Object? isLoadingAddFavouritesMovies = freezed,
    Object? failureAddingFavouritesMovies = freezed,
    Object? isLoadingRemoveFavouritesMovies = freezed,
    Object? failureRemovingFavouritesMovies = freezed,
  }) {
    return _then(_MoviesState(
      isLoadingMovies: isLoadingMovies == freezed
          ? _value.isLoadingMovies
          : isLoadingMovies // ignore: cast_nullable_to_non_nullable
              as bool?,
      failureGettingMovies: failureGettingMovies == freezed
          ? _value.failureGettingMovies
          : failureGettingMovies // ignore: cast_nullable_to_non_nullable
              as String?,
      movies: movies == freezed
          ? _value.movies
          : movies // ignore: cast_nullable_to_non_nullable
              as List<Movie>?,
      isLoadingFavouritesMovies: isLoadingFavouritesMovies == freezed
          ? _value.isLoadingFavouritesMovies
          : isLoadingFavouritesMovies // ignore: cast_nullable_to_non_nullable
              as bool?,
      failureGettingFavouritesMovies: failureGettingFavouritesMovies == freezed
          ? _value.failureGettingFavouritesMovies
          : failureGettingFavouritesMovies // ignore: cast_nullable_to_non_nullable
              as String?,
      favouriteMovies: favouriteMovies == freezed
          ? _value.favouriteMovies
          : favouriteMovies // ignore: cast_nullable_to_non_nullable
              as List<Movie>?,
      isLoadingAddFavouritesMovies: isLoadingAddFavouritesMovies == freezed
          ? _value.isLoadingAddFavouritesMovies
          : isLoadingAddFavouritesMovies // ignore: cast_nullable_to_non_nullable
              as bool?,
      failureAddingFavouritesMovies: failureAddingFavouritesMovies == freezed
          ? _value.failureAddingFavouritesMovies
          : failureAddingFavouritesMovies // ignore: cast_nullable_to_non_nullable
              as String?,
      isLoadingRemoveFavouritesMovies: isLoadingRemoveFavouritesMovies ==
              freezed
          ? _value.isLoadingRemoveFavouritesMovies
          : isLoadingRemoveFavouritesMovies // ignore: cast_nullable_to_non_nullable
              as bool?,
      failureRemovingFavouritesMovies: failureRemovingFavouritesMovies ==
              freezed
          ? _value.failureRemovingFavouritesMovies
          : failureRemovingFavouritesMovies // ignore: cast_nullable_to_non_nullable
              as String?,
    ));
  }
}

/// @nodoc

class _$_MoviesState implements _MoviesState {
  const _$_MoviesState(
      {this.isLoadingMovies,
      this.failureGettingMovies,
      final List<Movie>? movies,
      this.isLoadingFavouritesMovies,
      this.failureGettingFavouritesMovies,
      final List<Movie>? favouriteMovies,
      this.isLoadingAddFavouritesMovies,
      this.failureAddingFavouritesMovies,
      this.isLoadingRemoveFavouritesMovies,
      this.failureRemovingFavouritesMovies})
      : _movies = movies,
        _favouriteMovies = favouriteMovies;

  @override
  final bool? isLoadingMovies;
  @override
  final String? failureGettingMovies;
  final List<Movie>? _movies;
  @override
  List<Movie>? get movies {
    final value = _movies;
    if (value == null) return null;
    // ignore: implicit_dynamic_type
    return EqualUnmodifiableListView(value);
  }

//
  @override
  final bool? isLoadingFavouritesMovies;
  @override
  final String? failureGettingFavouritesMovies;
  final List<Movie>? _favouriteMovies;
  @override
  List<Movie>? get favouriteMovies {
    final value = _favouriteMovies;
    if (value == null) return null;
    // ignore: implicit_dynamic_type
    return EqualUnmodifiableListView(value);
  }

//
  @override
  final bool? isLoadingAddFavouritesMovies;
  @override
  final String? failureAddingFavouritesMovies;
//
  @override
  final bool? isLoadingRemoveFavouritesMovies;
  @override
  final String? failureRemovingFavouritesMovies;

  @override
  String toString() {
    return 'MoviesState(isLoadingMovies: $isLoadingMovies, failureGettingMovies: $failureGettingMovies, movies: $movies, isLoadingFavouritesMovies: $isLoadingFavouritesMovies, failureGettingFavouritesMovies: $failureGettingFavouritesMovies, favouriteMovies: $favouriteMovies, isLoadingAddFavouritesMovies: $isLoadingAddFavouritesMovies, failureAddingFavouritesMovies: $failureAddingFavouritesMovies, isLoadingRemoveFavouritesMovies: $isLoadingRemoveFavouritesMovies, failureRemovingFavouritesMovies: $failureRemovingFavouritesMovies)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _MoviesState &&
            const DeepCollectionEquality()
                .equals(other.isLoadingMovies, isLoadingMovies) &&
            const DeepCollectionEquality()
                .equals(other.failureGettingMovies, failureGettingMovies) &&
            const DeepCollectionEquality().equals(other.movies, movies) &&
            const DeepCollectionEquality().equals(
                other.isLoadingFavouritesMovies, isLoadingFavouritesMovies) &&
            const DeepCollectionEquality().equals(
                other.failureGettingFavouritesMovies,
                failureGettingFavouritesMovies) &&
            const DeepCollectionEquality()
                .equals(other.favouriteMovies, favouriteMovies) &&
            const DeepCollectionEquality().equals(
                other.isLoadingAddFavouritesMovies,
                isLoadingAddFavouritesMovies) &&
            const DeepCollectionEquality().equals(
                other.failureAddingFavouritesMovies,
                failureAddingFavouritesMovies) &&
            const DeepCollectionEquality().equals(
                other.isLoadingRemoveFavouritesMovies,
                isLoadingRemoveFavouritesMovies) &&
            const DeepCollectionEquality().equals(
                other.failureRemovingFavouritesMovies,
                failureRemovingFavouritesMovies));
  }

  @override
  int get hashCode => Object.hash(
      runtimeType,
      const DeepCollectionEquality().hash(isLoadingMovies),
      const DeepCollectionEquality().hash(failureGettingMovies),
      const DeepCollectionEquality().hash(movies),
      const DeepCollectionEquality().hash(isLoadingFavouritesMovies),
      const DeepCollectionEquality().hash(failureGettingFavouritesMovies),
      const DeepCollectionEquality().hash(favouriteMovies),
      const DeepCollectionEquality().hash(isLoadingAddFavouritesMovies),
      const DeepCollectionEquality().hash(failureAddingFavouritesMovies),
      const DeepCollectionEquality().hash(isLoadingRemoveFavouritesMovies),
      const DeepCollectionEquality().hash(failureRemovingFavouritesMovies));

  @JsonKey(ignore: true)
  @override
  _$MoviesStateCopyWith<_MoviesState> get copyWith =>
      __$MoviesStateCopyWithImpl<_MoviesState>(this, _$identity);
}

abstract class _MoviesState implements MoviesState {
  const factory _MoviesState(
      {final bool? isLoadingMovies,
      final String? failureGettingMovies,
      final List<Movie>? movies,
      final bool? isLoadingFavouritesMovies,
      final String? failureGettingFavouritesMovies,
      final List<Movie>? favouriteMovies,
      final bool? isLoadingAddFavouritesMovies,
      final String? failureAddingFavouritesMovies,
      final bool? isLoadingRemoveFavouritesMovies,
      final String? failureRemovingFavouritesMovies}) = _$_MoviesState;

  @override
  bool? get isLoadingMovies => throw _privateConstructorUsedError;
  @override
  String? get failureGettingMovies => throw _privateConstructorUsedError;
  @override
  List<Movie>? get movies => throw _privateConstructorUsedError;
  @override //
  bool? get isLoadingFavouritesMovies => throw _privateConstructorUsedError;
  @override
  String? get failureGettingFavouritesMovies =>
      throw _privateConstructorUsedError;
  @override
  List<Movie>? get favouriteMovies => throw _privateConstructorUsedError;
  @override //
  bool? get isLoadingAddFavouritesMovies => throw _privateConstructorUsedError;
  @override
  String? get failureAddingFavouritesMovies =>
      throw _privateConstructorUsedError;
  @override //
  bool? get isLoadingRemoveFavouritesMovies =>
      throw _privateConstructorUsedError;
  @override
  String? get failureRemovingFavouritesMovies =>
      throw _privateConstructorUsedError;
  @override
  @JsonKey(ignore: true)
  _$MoviesStateCopyWith<_MoviesState> get copyWith =>
      throw _privateConstructorUsedError;
}
