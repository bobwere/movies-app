import 'package:flutter/material.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:movies/presentation/auth/login/login_widget.dart';
import 'package:movies/presentation/widget/custom_input.dart';
import 'package:movies/services/auth/auth_provider/auth_provider.dart';
import 'package:movies/utils/input_validation_mixin.dart';

class ResetPasswordWidget extends ConsumerStatefulWidget {
  const ResetPasswordWidget({Key? key}) : super(key: key);

  @override
  _ResetPasswordWidgetState createState() => _ResetPasswordWidgetState();
}

class _ResetPasswordWidgetState extends ConsumerState<ResetPasswordWidget>
    with InputValidationMixin {
  final TextEditingController emailController = TextEditingController();
  bool formHasBeenSubmittedOnce = false;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: SafeArea(
        child: SingleChildScrollView(
          child: Column(
            children: <Widget>[
              SizedBox(
                height: 150,
              ),
              Align(
                child: Text(
                  'Reset Password',
                  style: Theme.of(context).textTheme.headline4!.copyWith(
                      fontWeight: FontWeight.w700,
                      fontSize: 24,
                      color: Colors.blueAccent),
                ),
              ),
              SizedBox(
                height: 20,
              ),
              CustomTextField(
                label: 'Email',
                hintText: 'e.g bobwere@gmail.com',
                iconData: Icons.email,
                errorText: formHasBeenSubmittedOnce
                    ? validate(emailController.text, 'Email')
                    : '',
                keyboardType: TextInputType.emailAddress,
                controller: emailController,
              ),
              SizedBox(
                height: 50,
              ),
              Padding(
                padding: EdgeInsets.symmetric(horizontal: 16),
                child: ElevatedButton(
                  onPressed: () {
                    setState(() {
                      formHasBeenSubmittedOnce = !formHasBeenSubmittedOnce;
                    });

                    if (isResetFormValid(email: emailController.text)) {
                      ref
                          .read(authProvider.notifier)
                          .resetPasword(email: emailController.text);
                    }

                    ScaffoldMessenger.of(context).showSnackBar(
                      const SnackBar(
                        backgroundColor: Colors.black,
                        duration: Duration(seconds: 3),
                        content: Text(
                          'Kindly enter your email correctly',
                          style: TextStyle(
                            fontFamily: 'Amiko',
                            color: Colors.white,
                            fontWeight: FontWeight.normal,
                          ),
                        ),
                      ),
                    );
                  },
                  child: Builder(
                    builder: (BuildContext context) {
                      final bool loadingResetAccount = ref
                          .watch(authProvider)
                          .value!
                          .isLoadingResetPassword!;

                      if (loadingResetAccount) {
                        return const Center(
                          child: SizedBox(
                            height: 15,
                            width: 15,
                            child: Center(
                              child: CircularProgressIndicator(
                                color: Colors.white,
                              ),
                            ),
                          ),
                        );
                      }

                      return Row(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: const <Widget>[
                          Text(
                            'Reset password',
                          )
                        ],
                      );
                    },
                  ),
                ),
              ),
              SizedBox(
                height: 20,
              ),
              TextButton(
                onPressed: () {
                  Navigator.pop(context);
                  Navigator.of(context).push(
                    MaterialPageRoute(
                      builder: (context) => LoginWidget(),
                    ),
                  );
                },
                style: TextButton.styleFrom(
                  splashFactory: NoSplash.splashFactory,
                ),
                child: Padding(
                  padding: EdgeInsets.symmetric(horizontal: 16),
                  child: RichText(
                    textAlign: TextAlign.center,
                    text: TextSpan(
                      text: 'Already have an account? ',
                      style: Theme.of(context).textTheme.subtitle2,
                      children: <TextSpan>[
                        TextSpan(
                          text: 'Login.',
                          style: Theme.of(context)
                              .textTheme
                              .subtitle2!
                              .copyWith(fontWeight: FontWeight.w700),
                        )
                      ],
                    ),
                  ),
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
