import 'package:flutter/material.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:movies/presentation/auth/create_account/create_account.dart';
import 'package:movies/presentation/auth/reset_password/reset_password.dart';
import 'package:movies/presentation/home/main/main_page.dart';
import 'package:movies/presentation/widget/custom_input.dart';
import 'package:movies/services/auth/auth_provider/auth_provider.dart';
import 'package:movies/services/auth/auth_state/auth_state.dart';
import 'package:movies/services/movies/movies_provider/movies_provider.dart';
import 'package:movies/utils/input_validation_mixin.dart';

class LoginWidget extends ConsumerStatefulWidget {
  const LoginWidget({Key? key}) : super(key: key);

  @override
  _LoginWidgetState createState() => _LoginWidgetState();
}

class _LoginWidgetState extends ConsumerState<LoginWidget>
    with InputValidationMixin {
  final TextEditingController emailController = TextEditingController();
  final TextEditingController passwordController = TextEditingController();
  bool formHasBeenSubmittedOnce = false;

  @override
  Widget build(BuildContext context) {
    ref.listen(authProvider,
        (AsyncValue<AuthState>? p, AsyncValue<AuthState> c) {
      if (c.value?.appUser != null) {
        ref.read(moviesProvider.notifier).getMovies(searchTerm: 'harry potter');
        ref
            .read(moviesProvider.notifier)
            .getFavoriteMovies(c.value!.appUser!.id!);
        Navigator.pop(context);
        Navigator.of(context).push(
          MaterialPageRoute(
            builder: (context) => MainPage(),
          ),
        );
      }

      if (c.value!.failureLogin!.isNotEmpty) {
        ScaffoldMessenger.of(context).showSnackBar(
          SnackBar(
            backgroundColor: Colors.black,
            duration: Duration(seconds: 3),
            content: Text(
              c.value!.failureLogin!,
              style: TextStyle(
                fontFamily: 'Amiko',
                color: Colors.white,
                fontWeight: FontWeight.normal,
              ),
            ),
          ),
        );
      }
    });

    return Scaffold(
      body: SafeArea(
        child: SingleChildScrollView(
          child: Column(
            children: <Widget>[
              SizedBox(
                height: 100,
              ),
              Align(
                child: Text(
                  'Login',
                  style: Theme.of(context).textTheme.headline4!.copyWith(
                      fontWeight: FontWeight.w700,
                      fontSize: 24,
                      color: Colors.blueAccent),
                ),
              ),
              SizedBox(height: 10),
              CustomTextField(
                label: 'Email address',
                hintText: 'e.g bobwere@gmail.com',
                iconData: Icons.email_outlined,
                errorText: formHasBeenSubmittedOnce
                    ? validate(emailController.text, 'Email')
                    : '',
                keyboardType: TextInputType.emailAddress,
                controller: emailController,
              ),
              SizedBox(height: 20),
              CustomTextField(
                label: 'Password',
                hintText: 'e.g Yus_uf_&',
                iconData: Icons.lock,
                obscureText: true,
                errorText: formHasBeenSubmittedOnce
                    ? validate(passwordController.text, 'Password')
                    : '',
                keyboardType: TextInputType.emailAddress,
                controller: passwordController,
              ),
              SizedBox(
                height: 10,
              ),
              Align(
                alignment: Alignment.centerRight,
                child: Padding(
                  padding: const EdgeInsets.only(right: 16.0),
                  child: TextButton(
                    onPressed: () {
                      Navigator.pop(context);
                      Navigator.of(context).push(
                        MaterialPageRoute(
                          builder: (context) => ResetPasswordWidget(),
                        ),
                      );
                    },
                    style: TextButton.styleFrom(
                      splashFactory: NoSplash.splashFactory,
                    ),
                    child: RichText(
                      text: TextSpan(
                        text: 'Forgot your',
                        style: Theme.of(context).textTheme.subtitle2,
                        children: <TextSpan>[
                          TextSpan(
                            text: ' password?',
                            style: Theme.of(context)
                                .textTheme
                                .subtitle2!
                                .copyWith(fontWeight: FontWeight.w600),
                          )
                        ],
                      ),
                    ),
                  ),
                ),
              ),
              SizedBox(height: 30),
              Padding(
                padding: EdgeInsets.symmetric(horizontal: 16),
                child: ElevatedButton(
                  style: ButtonStyle(
                    backgroundColor: MaterialStateProperty.all(Colors.black87),
                  ),
                  onPressed: () {
                    setState(() {
                      formHasBeenSubmittedOnce = !formHasBeenSubmittedOnce;
                    });

                    if (isEmailValid(emailController.text) &&
                        isPasswordValid(passwordController.text)) {
                      ref.read(authProvider.notifier).login(
                          email: emailController.text,
                          password: passwordController.text);
                      return;
                    }

                    ScaffoldMessenger.of(context).showSnackBar(
                      const SnackBar(
                        backgroundColor: Colors.black,
                        duration: Duration(seconds: 3),
                        content: Text(
                          'Kindly fill in the form correctly',
                          style: TextStyle(
                            fontFamily: 'Amiko',
                            color: Colors.white,
                            fontWeight: FontWeight.normal,
                          ),
                        ),
                      ),
                    );
                  },
                  child: Builder(
                    builder: (BuildContext context) {
                      final bool isLoadingLogin =
                          ref.watch(authProvider).value!.isLoadingLogin!;

                      if (isLoadingLogin) {
                        return const Center(
                          child: SizedBox(
                            height: 15,
                            width: 15,
                            child: Center(
                              child: CircularProgressIndicator(
                                color: Colors.white,
                              ),
                            ),
                          ),
                        );
                      }

                      return Row(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: const <Widget>[
                          Text(
                            'Login',
                          )
                        ],
                      );
                    },
                  ),
                ),
              ),
              SizedBox(
                height: 100,
              ),
              TextButton(
                onPressed: () {
                  Navigator.pop(context);
                  Navigator.of(context).push(
                    MaterialPageRoute(
                      builder: (context) => CreateAccountWidget(),
                    ),
                  );
                },
                style: TextButton.styleFrom(
                  splashFactory: NoSplash.splashFactory,
                ),
                child: Padding(
                  padding: EdgeInsets.symmetric(horizontal: 16),
                  child: RichText(
                    textAlign: TextAlign.center,
                    text: TextSpan(
                      text: 'Don\'t have an account? ',
                      style: Theme.of(context).textTheme.subtitle2,
                      children: <TextSpan>[
                        TextSpan(
                          text: 'Sign Up.',
                          style: Theme.of(context)
                              .textTheme
                              .subtitle2!
                              .copyWith(fontWeight: FontWeight.w700),
                        )
                      ],
                    ),
                  ),
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
