import 'package:flutter/material.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:movies/presentation/auth/login/login_widget.dart';
import 'package:movies/presentation/home/main/main_page.dart';
import 'package:movies/presentation/widget/custom_input.dart';
import 'package:movies/services/auth/auth_provider/auth_provider.dart';
import 'package:movies/services/auth/auth_state/auth_state.dart';
import 'package:movies/services/movies/movies_provider/movies_provider.dart';
import 'package:movies/utils/input_validation_mixin.dart';

class CreateAccountWidget extends ConsumerStatefulWidget {
  const CreateAccountWidget({Key? key}) : super(key: key);

  @override
  _CreateAccountWidgetState createState() => _CreateAccountWidgetState();
}

class _CreateAccountWidgetState extends ConsumerState<CreateAccountWidget>
    with InputValidationMixin {
  final TextEditingController emailController = TextEditingController();
  final TextEditingController passwordController = TextEditingController();
  bool formHasBeenSubmittedOnce = false;

  @override
  Widget build(BuildContext context) {
    ref.listen(authProvider,
        (AsyncValue<AuthState>? p, AsyncValue<AuthState> c) {
      if (c.value?.appUser != null) {
        ref.read(moviesProvider.notifier).getMovies(searchTerm: 'harry potter');
        ref
            .read(moviesProvider.notifier)
            .getFavoriteMovies(c.value!.appUser!.id!);
        Navigator.pop(context);
        Navigator.of(context).push(
          MaterialPageRoute(
            builder: (context) => MainPage(),
          ),
        );
      }

      if (c.value!.failureCreateAccount!.isNotEmpty) {
        ScaffoldMessenger.of(context).showSnackBar(
          SnackBar(
            backgroundColor: Colors.black,
            duration: Duration(seconds: 3),
            content: Text(
              c.value!.failureCreateAccount!,
              style: TextStyle(
                fontFamily: 'Amiko',
                color: Colors.white,
                fontWeight: FontWeight.normal,
              ),
            ),
          ),
        );
      }
    });

    return Scaffold(
      body: SafeArea(
        child: SingleChildScrollView(
          child: Column(
            children: <Widget>[
              SizedBox(
                height: 100,
              ),
              Align(
                child: Text(
                  'Create your account',
                  style: Theme.of(context).textTheme.headline4!.copyWith(
                      fontWeight: FontWeight.w700,
                      fontSize: 24,
                      color: Colors.blueAccent),
                ),
              ),
              SizedBox(height: 10),
              CustomTextField(
                label: 'Email address',
                hintText: 'e.g bobwere@gmail.com',
                iconData: Icons.email_outlined,
                errorText: formHasBeenSubmittedOnce
                    ? validate(emailController.text, 'Email')
                    : '',
                keyboardType: TextInputType.emailAddress,
                controller: emailController,
              ),
              SizedBox(height: 20),
              CustomTextField(
                label: 'Password',
                hintText: 'e.g Yus_uf_&',
                iconData: Icons.lock,
                obscureText: true,
                errorText: formHasBeenSubmittedOnce
                    ? validate(passwordController.text, 'Password')
                    : '',
                keyboardType: TextInputType.emailAddress,
                controller: passwordController,
              ),
              SizedBox(height: 30),
              Padding(
                padding: EdgeInsets.symmetric(horizontal: 16),
                child: ElevatedButton(
                  style: ButtonStyle(
                    backgroundColor: MaterialStateProperty.all(Colors.black87),
                  ),
                  onPressed: () {
                    setState(() {
                      formHasBeenSubmittedOnce = !formHasBeenSubmittedOnce;
                    });

                    if (isEmailValid(emailController.text) &&
                        isPasswordValid(passwordController.text)) {
                      ref.read(authProvider.notifier).createAccount(
                          email: emailController.text,
                          password: passwordController.text);
                      return;
                    }

                    ScaffoldMessenger.of(context).showSnackBar(
                      const SnackBar(
                        backgroundColor: Colors.black,
                        duration: Duration(seconds: 3),
                        content: Text(
                          'Kindly fill in the form correctly',
                          style: TextStyle(
                            fontFamily: 'Amiko',
                            color: Colors.white,
                            fontWeight: FontWeight.normal,
                          ),
                        ),
                      ),
                    );
                  },
                  child: ref.watch(authProvider).value!.isLoadingCreateAccount!
                      ? Center(
                          child: SizedBox(
                            height: 15,
                            width: 15,
                            child: Center(
                              child: CircularProgressIndicator(
                                color: Colors.white,
                              ),
                            ),
                          ),
                        )
                      : Row(
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: const <Widget>[
                            Text(
                              'Create account',
                            )
                          ],
                        ),
                ),
              ),
              SizedBox(
                height: 100,
              ),
              TextButton(
                onPressed: () {
                  Navigator.pop(context);
                  Navigator.of(context).push(
                    MaterialPageRoute(
                      builder: (context) => LoginWidget(),
                    ),
                  );
                },
                style: TextButton.styleFrom(
                  splashFactory: NoSplash.splashFactory,
                ),
                child: Padding(
                  padding: EdgeInsets.symmetric(horizontal: 16),
                  child: RichText(
                    textAlign: TextAlign.center,
                    text: TextSpan(
                      text: 'Already have an account? ',
                      style: Theme.of(context).textTheme.subtitle2,
                      children: <TextSpan>[
                        TextSpan(
                          text: 'Login.',
                          style: Theme.of(context)
                              .textTheme
                              .subtitle2!
                              .copyWith(fontWeight: FontWeight.w700),
                        )
                      ],
                    ),
                  ),
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
