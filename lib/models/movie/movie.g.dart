// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'movie.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

_$_Movie _$$_MovieFromJson(Map<String, dynamic> json) => _$_Movie(
      title: json['Title'] as String? ?? '',
      year: json['Year'] as String? ?? '',
      poster: json['Poster'] as String? ?? '',
      id: json['imdbID'] as String? ?? '',
      type: json['Type'] as String? ?? '',
    );

Map<String, dynamic> _$$_MovieToJson(_$_Movie instance) => <String, dynamic>{
      'Title': instance.title,
      'Year': instance.year,
      'Poster': instance.poster,
      'imdbID': instance.id,
      'Type': instance.type,
    };
