import 'package:flutter/material.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:movies/presentation/home/favourites_page/favourites_page.dart';
import 'package:movies/presentation/home/movies_page/movies_page.dart';
import 'package:movies/services/bottom_navigation/bottom_navigation_state.dart';
import 'package:movies/services/movies/movies_provider/movies_provider.dart';

class MainPage extends ConsumerWidget {
  const MainPage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context, WidgetRef ref) {
    int currentIndex = ref.watch(pageIndexProvider);
    return Scaffold(
      body: SafeArea(
        child: currentIndex == 0 ? MoviePage() : FavouritesPage(),
      ),
      bottomNavigationBar: BottomNavigationBar(
          currentIndex: currentIndex,
          onTap: (int index) {
            ref.read(pageIndexProvider.notifier).state = index;
          },
          items: [
            BottomNavigationBarItem(
              label: '',
              icon: Icon(Icons.movie_creation_outlined),
            ),
            BottomNavigationBarItem(
                label: '',
                icon: Icon(
                  Icons.favorite_border,
                ))
          ]),
    );
  }
}
