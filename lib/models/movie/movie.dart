import 'package:freezed_annotation/freezed_annotation.dart';
import 'package:flutter/foundation.dart';

part 'movie.freezed.dart';
part 'movie.g.dart';

@freezed
class Movie with _$Movie {
  const factory Movie({
    @JsonKey(name: 'Title', defaultValue: '') String? title,
    @JsonKey(name: 'Year', defaultValue: '') String? year,
    @JsonKey(name: 'Poster', defaultValue: '') String? poster,
    @JsonKey(name: 'imdbID', defaultValue: '') String? id,
    @JsonKey(name: 'Type', defaultValue: '') String? type,
  }) = _Movie;

  factory Movie.fromJson(Map<String, dynamic> json) => _$MovieFromJson(json);

  factory Movie.initial() => Movie();
}
