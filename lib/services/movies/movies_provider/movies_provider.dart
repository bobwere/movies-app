import 'dart:convert';

import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:movies/models/movie/movie.dart';
import 'package:movies/services/movies/movies_state/movies_state.dart';
import 'package:http/http.dart' as http;

class MoviesNotifier extends StateNotifier<AsyncValue<MoviesState>> {
  MoviesNotifier() : super(AsyncValue.data(MoviesState.initial()));

  Future<void> getMovies({required String searchTerm}) async {
    try {
      state = AsyncValue.data(state.value!.copyWith
          .call(failureGettingMovies: '', isLoadingMovies: true));

      //fetching movies
      http.Response response = await http.get(
          Uri.parse("http://www.omdbapi.com/?s=$searchTerm&apikey=e3b44f86"));

      Map<String, dynamic> results = jsonDecode(response.body);

      //act on response if successful
      if (results['Search'] != null) {
        List<Movie> movies = <Movie>[];

        List<dynamic> movieResults = results['Search'] as List<dynamic>;

        for (var result in movieResults) {
          movies.add(Movie.fromJson(result as Map<String, dynamic>));
        }

        state = AsyncValue.data(state.value!.copyWith.call(
            failureGettingMovies: '', movies: movies, isLoadingMovies: false));
      }

      //act on response if unsuccessful
      if (results['Response'] != 'True') {
        state = AsyncValue.data(state.value!.copyWith.call(
            failureGettingMovies: results['Error'], isLoadingMovies: false));
      }
    } catch (e) {
      state = AsyncValue.data(state.value!.copyWith.call(
          failureGettingMovies: 'Server failure occurred',
          isLoadingMovies: false));
    }
  }

  Future<void> getFavoriteMovies(String userID) async {
    try {
      state = AsyncValue.data(state.value!.copyWith.call(
          failureGettingFavouritesMovies: '', isLoadingFavouritesMovies: true));

      final QuerySnapshot<Map<String, dynamic>> collectionStream =
          await FirebaseFirestore.instance
              .collection('favourites')
              .doc(userID)
              .collection('myfavourites')
              .get();

      final List<Movie> movies = collectionStream.docs.map((e) {
        final Map<String, dynamic> data = e.data();
        return Movie.fromJson(data);
      }).toList();

      state = AsyncValue.data(state.value!.copyWith.call(
        failureGettingFavouritesMovies: '',
        isLoadingFavouritesMovies: false,
        favouriteMovies: movies,
      ));
    } catch (e) {
      state = AsyncValue.data(state.value!.copyWith.call(
          failureGettingFavouritesMovies: 'Server failure occurred',
          isLoadingFavouritesMovies: false));
    }
  }

  Future<void> addMovieToFavorites(String userID, Movie movie) async {
    try {
      state = AsyncValue.data(state.value!.copyWith.call(
          failureAddingFavouritesMovies: '',
          isLoadingAddFavouritesMovies: true));

      await FirebaseFirestore.instance
          .collection('favourites')
          .doc(userID)
          .collection('myfavourites')
          .doc(movie.id)
          .set(movie.toJson());

      await getFavoriteMovies(userID);

      state = AsyncValue.data(state.value!.copyWith.call(
          failureAddingFavouritesMovies: '',
          isLoadingAddFavouritesMovies: false));
    } catch (e) {
      state = AsyncValue.data(state.value!.copyWith.call(
          failureAddingFavouritesMovies: '',
          isLoadingAddFavouritesMovies: false));
    }
  }

  Future<void> removeMovieToFavorite(String userID, Movie movie) async {
    try {
      state = AsyncValue.data(state.value!.copyWith.call(
          failureRemovingFavouritesMovies: '',
          isLoadingRemoveFavouritesMovies: true));

      await FirebaseFirestore.instance
          .collection('favourites')
          .doc(userID)
          .collection('myfavourites')
          .doc(movie.id)
          .delete();

      await getFavoriteMovies(userID);

      state = AsyncValue.data(state.value!.copyWith.call(
          failureRemovingFavouritesMovies: '',
          isLoadingRemoveFavouritesMovies: false));
    } catch (e) {
      state = AsyncValue.data(state.value!.copyWith.call(
          failureRemovingFavouritesMovies: '',
          isLoadingRemoveFavouritesMovies: false));
    }
  }
}

final moviesProvider =
    StateNotifierProvider<MoviesNotifier, AsyncValue<MoviesState>>(
        (ref) => MoviesNotifier());
