import 'package:freezed_annotation/freezed_annotation.dart';
import 'package:movies/models/movie/movie.dart';

part 'movies_state.freezed.dart';

@freezed
class MoviesState with _$MoviesState {
  const factory MoviesState({
    bool? isLoadingMovies,
    String? failureGettingMovies,
    List<Movie>? movies,
    //
    bool? isLoadingFavouritesMovies,
    String? failureGettingFavouritesMovies,
    List<Movie>? favouriteMovies,
    //
    bool? isLoadingAddFavouritesMovies,
    String? failureAddingFavouritesMovies,
    //
    bool? isLoadingRemoveFavouritesMovies,
    String? failureRemovingFavouritesMovies,
  }) = _MoviesState;

  factory MoviesState.initial() => MoviesState(
        failureGettingMovies: '',
        isLoadingMovies: false,
        movies: <Movie>[],
        failureGettingFavouritesMovies: '',
        favouriteMovies: <Movie>[],
        isLoadingFavouritesMovies: false,
        failureAddingFavouritesMovies: '',
        failureRemovingFavouritesMovies: '',
        isLoadingAddFavouritesMovies: false,
        isLoadingRemoveFavouritesMovies: false,
      );
}
