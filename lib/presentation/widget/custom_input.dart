import 'package:flutter/material.dart';

class CustomTextField extends StatefulWidget {
  const CustomTextField({
    Key? key,
    required this.label,
    required this.hintText,
    required this.errorText,
    required this.keyboardType,
    required this.controller,
    required this.iconData,
    this.obscureText = false,
    this.maxLines = 1,
  }) : super(key: key);

  final String label;
  final String hintText;
  final TextEditingController controller;
  final IconData iconData;
  final bool obscureText;
  final int? maxLines;

  final String errorText;
  final TextInputType keyboardType;

  @override
  State<CustomTextField> createState() => _CustomTextFieldState();
}

class _CustomTextFieldState extends State<CustomTextField> {
  @override
  Widget build(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: <Widget>[
        Padding(
          padding: EdgeInsets.symmetric(horizontal: 16),
          child: TextField(
            keyboardType: widget.keyboardType,
            controller: widget.controller,
            obscureText: widget.obscureText,
            maxLines: widget.maxLines,
            decoration: InputDecoration(
              prefixIcon: Icon(
                widget.iconData,
                size: 25,
                color: const Color(0xFFA3BBD7),
              ),
              isDense: true,
              contentPadding: EdgeInsets.zero,
              labelText: widget.label,
              hintText: widget.hintText,
            ),
          ),
        ),
        if (widget.errorText.isNotEmpty) ...<Widget>[
          SizedBox(
            height: 5,
          ),
          Padding(
            padding: EdgeInsets.symmetric(horizontal: 16),
            child: Text(
              widget.errorText,
              style: const TextStyle(
                color: Color(0xFFEA2727),
                fontFamily: 'Amiko',
                fontSize: 12,
              ),
            ),
          ),
        ]
      ],
    );
  }
}
