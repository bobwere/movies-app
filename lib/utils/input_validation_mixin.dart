mixin InputValidationMixin {
  bool isPasswordValid(String value) => value.length >= 6;

  bool isEmailValid(String value) {
    const String pattern =
        r'^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$';
    final RegExp regex = RegExp(pattern);

    return regex.hasMatch(value);
  }

  bool isEmpty(String value) => value.isEmpty;

  String validate(String value, String inputLabel) {
    if (inputLabel == 'Name') {
      if (isEmpty(value)) {
        return 'Please enter your name';
      }
      return '';
    }

    if (inputLabel == 'Alias') {
      if (isEmpty(value)) {
        return 'Please enter your alias';
      }
      return '';
    }

    if (inputLabel == 'Email') {
      if (isEmpty(value)) {
        return 'Please enter your email address';
      }
      if (!isEmailValid(value)) {
        return 'This email address is invalid';
      }
      return '';
    }

    if (inputLabel == 'Number') {
      if (isEmpty(value)) {
        return 'Please enter your phone number';
      }
      if (value.length != 10) {
        return 'Please enter a 10 digit number';
      }
      return '';
    }

    if (inputLabel == 'Password') {
      if (isEmpty(value)) {
        return 'Please enter your password';
      }
      if (!isPasswordValid(value)) {
        return 'Minimum password length is 6';
      }
      return '';
    }

    return '';
  }

  bool isCreateAccountFormValid({
    required String name,
    required String alias,
    required String email,
    required String password,
    required String phone,
  }) {
    final String _name = validate(name, 'Name');
    final String _alias = validate(alias, 'Alias');
    final String _email = validate(email, 'Email');
    final String _password = validate(password, 'Password');
    final String _phone = validate(phone, 'Number');

    if (_name.isEmpty &&
        _alias.isEmpty &&
        _email.isEmpty &&
        _password.isEmpty &&
        _phone.isEmpty) {
      return true;
    }
    return false;
  }

  bool isLoginFormValid({
    required String password,
    required String email,
  }) {
    final String _email = validate(email, 'Email');
    final String _password = validate(password, 'Password');

    if (_email.isEmpty && _password.isEmpty) {
      return true;
    }
    return false;
  }

  bool isResetFormValid({
    required String email,
  }) {
    final String _email = validate(email, 'Email');

    if (_email.isEmpty) {
      return true;
    }
    return false;
  }
}
