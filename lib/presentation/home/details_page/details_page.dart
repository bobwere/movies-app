import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:movies/models/movie/movie.dart';
import 'package:movies/presentation/auth/login/login_widget.dart';
import 'package:movies/presentation/home/main/main_page.dart';
import 'package:movies/services/auth/auth_provider/auth_provider.dart';
import 'package:movies/services/movies/movies_provider/movies_provider.dart';

import '../../../services/auth/auth_state/auth_state.dart';

class DetailsPage extends ConsumerWidget {
  const DetailsPage({
    Key? key,
    required this.movie,
  }) : super(key: key);

  final Movie movie;

  @override
  Widget build(BuildContext context, WidgetRef ref) {
    bool isFavorited =
        ref.watch(moviesProvider).value!.favouriteMovies!.contains(movie);

    ref.listen(authProvider,
        (AsyncValue<AuthState>? p, AsyncValue<AuthState> c) {
      if (c.value?.appUser == null) {
        Navigator.of(context).pop();
        Navigator.of(context).push(
          MaterialPageRoute(
            builder: (context) => LoginWidget(),
          ),
        );
      }
    });

    return Scaffold(
      body: SafeArea(
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.center,
          children: [
            Align(
              alignment: Alignment.centerLeft,
              child: Padding(
                padding: const EdgeInsets.all(8.0),
                child: IconButton(
                  onPressed: () {
                    Navigator.pop(context);
                  },
                  icon: Icon(Icons.arrow_back_ios),
                ),
              ),
            ),
            SizedBox(
              height: 30,
            ),
            Align(
              alignment: Alignment.center,
              child: Container(
                height: 150,
                width: 150,
                child: ClipRRect(
                  borderRadius: BorderRadius.circular(10000),
                  child: CachedNetworkImage(
                    imageUrl: movie.poster ?? '',
                    fit: BoxFit.fitWidth,
                  ),
                ),
              ),
            ),
            SizedBox(height: 50),
            Padding(
              padding: const EdgeInsets.all(8.0),
              child: Text(
                movie.title ?? "",
                textAlign: TextAlign.center,
                style: TextStyle(
                  fontWeight: FontWeight.bold,
                  fontSize: 20,
                ),
              ),
            ),
            Padding(
              padding: const EdgeInsets.all(8.0),
              child: Text(
                movie.year ?? "",
                textAlign: TextAlign.center,
                style: TextStyle(
                    fontWeight: FontWeight.w500,
                    fontSize: 17,
                    color: Colors.grey),
              ),
            ),
            Padding(
              padding: const EdgeInsets.all(20.0),
              child: SizedBox(
                width: MediaQuery.of(context).size.width,
                height: 45,
                child: ElevatedButton(
                    onPressed: () {
                      if (isFavorited) {
                        ref.read(moviesProvider.notifier).removeMovieToFavorite(
                            ref.read(authProvider).value!.appUser!.id!, movie);
                      }
                      if (!isFavorited) {
                        ref.read(moviesProvider.notifier).addMovieToFavorites(
                            ref.read(authProvider).value!.appUser!.id!, movie);
                      }
                    },
                    style: ButtonStyle(
                        elevation: MaterialStateProperty.all(0),
                        backgroundColor:
                            MaterialStateProperty.all(Colors.black87)),
                    child: Center(
                      child: ref
                                  .read(moviesProvider)
                                  .value!
                                  .isLoadingAddFavouritesMovies! ||
                              ref
                                  .read(moviesProvider)
                                  .value!
                                  .isLoadingRemoveFavouritesMovies!
                          ? SizedBox(
                              height: 15,
                              width: 15,
                              child: CircularProgressIndicator(
                                color: Colors.white,
                              ),
                            )
                          : Text(!isFavorited
                              ? 'Add to Favorite'
                              : 'Remove from favourites'),
                    )),
              ),
            ),
            Padding(
              padding: const EdgeInsets.symmetric(horizontal: 20.0),
              child: SizedBox(
                width: MediaQuery.of(context).size.width,
                height: 45,
                child: ElevatedButton(
                    onPressed: () {
                      ref.read(authProvider.notifier).logOut();
                    },
                    style: ButtonStyle(
                        elevation: MaterialStateProperty.all(0),
                        backgroundColor:
                            MaterialStateProperty.all(Colors.black87)),
                    child: Center(
                      child: Text('Log out'),
                    )),
              ),
            )
          ],
        ),
      ),
    );
  }
}
