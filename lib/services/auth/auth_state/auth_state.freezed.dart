// coverage:ignore-file
// GENERATED CODE - DO NOT MODIFY BY HAND
// ignore_for_file: type=lint
// ignore_for_file: unused_element, deprecated_member_use, deprecated_member_use_from_same_package, use_function_type_syntax_for_parameters, unnecessary_const, avoid_init_to_null, invalid_override_different_default_values_named, prefer_expression_function_bodies, annotate_overrides, invalid_annotation_target

part of 'auth_state.dart';

// **************************************************************************
// FreezedGenerator
// **************************************************************************

T _$identity<T>(T value) => value;

final _privateConstructorUsedError = UnsupportedError(
    'It seems like you constructed your class using `MyClass._()`. This constructor is only meant to be used by freezed and you are not supposed to need it nor use it.\nPlease check the documentation here for more information: https://github.com/rrousselGit/freezed#custom-getters-and-methods');

/// @nodoc
mixin _$AuthState {
//
  bool? get isLoadingLogin => throw _privateConstructorUsedError;
  bool? get isLoadingCreateAccount => throw _privateConstructorUsedError;
  bool? get isLoadingResetPassword => throw _privateConstructorUsedError; //
  String? get failureLogin => throw _privateConstructorUsedError;
  String? get failureResetPassword => throw _privateConstructorUsedError;
  String? get failureCreateAccount => throw _privateConstructorUsedError; //
  String? get email => throw _privateConstructorUsedError;
  String? get password => throw _privateConstructorUsedError; //
  bool? get emailIsValid => throw _privateConstructorUsedError;
  bool? get passwordIsValid => throw _privateConstructorUsedError; //
  bool? get hasAttemptedFirstSubmit => throw _privateConstructorUsedError; //
  AppUser? get appUser => throw _privateConstructorUsedError; //
  bool? get hasSentResetLink => throw _privateConstructorUsedError;

  @JsonKey(ignore: true)
  $AuthStateCopyWith<AuthState> get copyWith =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $AuthStateCopyWith<$Res> {
  factory $AuthStateCopyWith(AuthState value, $Res Function(AuthState) then) =
      _$AuthStateCopyWithImpl<$Res>;
  $Res call(
      {bool? isLoadingLogin,
      bool? isLoadingCreateAccount,
      bool? isLoadingResetPassword,
      String? failureLogin,
      String? failureResetPassword,
      String? failureCreateAccount,
      String? email,
      String? password,
      bool? emailIsValid,
      bool? passwordIsValid,
      bool? hasAttemptedFirstSubmit,
      AppUser? appUser,
      bool? hasSentResetLink});

  $AppUserCopyWith<$Res>? get appUser;
}

/// @nodoc
class _$AuthStateCopyWithImpl<$Res> implements $AuthStateCopyWith<$Res> {
  _$AuthStateCopyWithImpl(this._value, this._then);

  final AuthState _value;
  // ignore: unused_field
  final $Res Function(AuthState) _then;

  @override
  $Res call({
    Object? isLoadingLogin = freezed,
    Object? isLoadingCreateAccount = freezed,
    Object? isLoadingResetPassword = freezed,
    Object? failureLogin = freezed,
    Object? failureResetPassword = freezed,
    Object? failureCreateAccount = freezed,
    Object? email = freezed,
    Object? password = freezed,
    Object? emailIsValid = freezed,
    Object? passwordIsValid = freezed,
    Object? hasAttemptedFirstSubmit = freezed,
    Object? appUser = freezed,
    Object? hasSentResetLink = freezed,
  }) {
    return _then(_value.copyWith(
      isLoadingLogin: isLoadingLogin == freezed
          ? _value.isLoadingLogin
          : isLoadingLogin // ignore: cast_nullable_to_non_nullable
              as bool?,
      isLoadingCreateAccount: isLoadingCreateAccount == freezed
          ? _value.isLoadingCreateAccount
          : isLoadingCreateAccount // ignore: cast_nullable_to_non_nullable
              as bool?,
      isLoadingResetPassword: isLoadingResetPassword == freezed
          ? _value.isLoadingResetPassword
          : isLoadingResetPassword // ignore: cast_nullable_to_non_nullable
              as bool?,
      failureLogin: failureLogin == freezed
          ? _value.failureLogin
          : failureLogin // ignore: cast_nullable_to_non_nullable
              as String?,
      failureResetPassword: failureResetPassword == freezed
          ? _value.failureResetPassword
          : failureResetPassword // ignore: cast_nullable_to_non_nullable
              as String?,
      failureCreateAccount: failureCreateAccount == freezed
          ? _value.failureCreateAccount
          : failureCreateAccount // ignore: cast_nullable_to_non_nullable
              as String?,
      email: email == freezed
          ? _value.email
          : email // ignore: cast_nullable_to_non_nullable
              as String?,
      password: password == freezed
          ? _value.password
          : password // ignore: cast_nullable_to_non_nullable
              as String?,
      emailIsValid: emailIsValid == freezed
          ? _value.emailIsValid
          : emailIsValid // ignore: cast_nullable_to_non_nullable
              as bool?,
      passwordIsValid: passwordIsValid == freezed
          ? _value.passwordIsValid
          : passwordIsValid // ignore: cast_nullable_to_non_nullable
              as bool?,
      hasAttemptedFirstSubmit: hasAttemptedFirstSubmit == freezed
          ? _value.hasAttemptedFirstSubmit
          : hasAttemptedFirstSubmit // ignore: cast_nullable_to_non_nullable
              as bool?,
      appUser: appUser == freezed
          ? _value.appUser
          : appUser // ignore: cast_nullable_to_non_nullable
              as AppUser?,
      hasSentResetLink: hasSentResetLink == freezed
          ? _value.hasSentResetLink
          : hasSentResetLink // ignore: cast_nullable_to_non_nullable
              as bool?,
    ));
  }

  @override
  $AppUserCopyWith<$Res>? get appUser {
    if (_value.appUser == null) {
      return null;
    }

    return $AppUserCopyWith<$Res>(_value.appUser!, (value) {
      return _then(_value.copyWith(appUser: value));
    });
  }
}

/// @nodoc
abstract class _$AuthStateCopyWith<$Res> implements $AuthStateCopyWith<$Res> {
  factory _$AuthStateCopyWith(
          _AuthState value, $Res Function(_AuthState) then) =
      __$AuthStateCopyWithImpl<$Res>;
  @override
  $Res call(
      {bool? isLoadingLogin,
      bool? isLoadingCreateAccount,
      bool? isLoadingResetPassword,
      String? failureLogin,
      String? failureResetPassword,
      String? failureCreateAccount,
      String? email,
      String? password,
      bool? emailIsValid,
      bool? passwordIsValid,
      bool? hasAttemptedFirstSubmit,
      AppUser? appUser,
      bool? hasSentResetLink});

  @override
  $AppUserCopyWith<$Res>? get appUser;
}

/// @nodoc
class __$AuthStateCopyWithImpl<$Res> extends _$AuthStateCopyWithImpl<$Res>
    implements _$AuthStateCopyWith<$Res> {
  __$AuthStateCopyWithImpl(_AuthState _value, $Res Function(_AuthState) _then)
      : super(_value, (v) => _then(v as _AuthState));

  @override
  _AuthState get _value => super._value as _AuthState;

  @override
  $Res call({
    Object? isLoadingLogin = freezed,
    Object? isLoadingCreateAccount = freezed,
    Object? isLoadingResetPassword = freezed,
    Object? failureLogin = freezed,
    Object? failureResetPassword = freezed,
    Object? failureCreateAccount = freezed,
    Object? email = freezed,
    Object? password = freezed,
    Object? emailIsValid = freezed,
    Object? passwordIsValid = freezed,
    Object? hasAttemptedFirstSubmit = freezed,
    Object? appUser = freezed,
    Object? hasSentResetLink = freezed,
  }) {
    return _then(_AuthState(
      isLoadingLogin: isLoadingLogin == freezed
          ? _value.isLoadingLogin
          : isLoadingLogin // ignore: cast_nullable_to_non_nullable
              as bool?,
      isLoadingCreateAccount: isLoadingCreateAccount == freezed
          ? _value.isLoadingCreateAccount
          : isLoadingCreateAccount // ignore: cast_nullable_to_non_nullable
              as bool?,
      isLoadingResetPassword: isLoadingResetPassword == freezed
          ? _value.isLoadingResetPassword
          : isLoadingResetPassword // ignore: cast_nullable_to_non_nullable
              as bool?,
      failureLogin: failureLogin == freezed
          ? _value.failureLogin
          : failureLogin // ignore: cast_nullable_to_non_nullable
              as String?,
      failureResetPassword: failureResetPassword == freezed
          ? _value.failureResetPassword
          : failureResetPassword // ignore: cast_nullable_to_non_nullable
              as String?,
      failureCreateAccount: failureCreateAccount == freezed
          ? _value.failureCreateAccount
          : failureCreateAccount // ignore: cast_nullable_to_non_nullable
              as String?,
      email: email == freezed
          ? _value.email
          : email // ignore: cast_nullable_to_non_nullable
              as String?,
      password: password == freezed
          ? _value.password
          : password // ignore: cast_nullable_to_non_nullable
              as String?,
      emailIsValid: emailIsValid == freezed
          ? _value.emailIsValid
          : emailIsValid // ignore: cast_nullable_to_non_nullable
              as bool?,
      passwordIsValid: passwordIsValid == freezed
          ? _value.passwordIsValid
          : passwordIsValid // ignore: cast_nullable_to_non_nullable
              as bool?,
      hasAttemptedFirstSubmit: hasAttemptedFirstSubmit == freezed
          ? _value.hasAttemptedFirstSubmit
          : hasAttemptedFirstSubmit // ignore: cast_nullable_to_non_nullable
              as bool?,
      appUser: appUser == freezed
          ? _value.appUser
          : appUser // ignore: cast_nullable_to_non_nullable
              as AppUser?,
      hasSentResetLink: hasSentResetLink == freezed
          ? _value.hasSentResetLink
          : hasSentResetLink // ignore: cast_nullable_to_non_nullable
              as bool?,
    ));
  }
}

/// @nodoc

class _$_AuthState implements _AuthState {
  const _$_AuthState(
      {this.isLoadingLogin,
      this.isLoadingCreateAccount,
      this.isLoadingResetPassword,
      this.failureLogin,
      this.failureResetPassword,
      this.failureCreateAccount,
      this.email,
      this.password,
      this.emailIsValid,
      this.passwordIsValid,
      this.hasAttemptedFirstSubmit,
      this.appUser,
      this.hasSentResetLink});

//
  @override
  final bool? isLoadingLogin;
  @override
  final bool? isLoadingCreateAccount;
  @override
  final bool? isLoadingResetPassword;
//
  @override
  final String? failureLogin;
  @override
  final String? failureResetPassword;
  @override
  final String? failureCreateAccount;
//
  @override
  final String? email;
  @override
  final String? password;
//
  @override
  final bool? emailIsValid;
  @override
  final bool? passwordIsValid;
//
  @override
  final bool? hasAttemptedFirstSubmit;
//
  @override
  final AppUser? appUser;
//
  @override
  final bool? hasSentResetLink;

  @override
  String toString() {
    return 'AuthState(isLoadingLogin: $isLoadingLogin, isLoadingCreateAccount: $isLoadingCreateAccount, isLoadingResetPassword: $isLoadingResetPassword, failureLogin: $failureLogin, failureResetPassword: $failureResetPassword, failureCreateAccount: $failureCreateAccount, email: $email, password: $password, emailIsValid: $emailIsValid, passwordIsValid: $passwordIsValid, hasAttemptedFirstSubmit: $hasAttemptedFirstSubmit, appUser: $appUser, hasSentResetLink: $hasSentResetLink)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _AuthState &&
            const DeepCollectionEquality()
                .equals(other.isLoadingLogin, isLoadingLogin) &&
            const DeepCollectionEquality()
                .equals(other.isLoadingCreateAccount, isLoadingCreateAccount) &&
            const DeepCollectionEquality()
                .equals(other.isLoadingResetPassword, isLoadingResetPassword) &&
            const DeepCollectionEquality()
                .equals(other.failureLogin, failureLogin) &&
            const DeepCollectionEquality()
                .equals(other.failureResetPassword, failureResetPassword) &&
            const DeepCollectionEquality()
                .equals(other.failureCreateAccount, failureCreateAccount) &&
            const DeepCollectionEquality().equals(other.email, email) &&
            const DeepCollectionEquality().equals(other.password, password) &&
            const DeepCollectionEquality()
                .equals(other.emailIsValid, emailIsValid) &&
            const DeepCollectionEquality()
                .equals(other.passwordIsValid, passwordIsValid) &&
            const DeepCollectionEquality().equals(
                other.hasAttemptedFirstSubmit, hasAttemptedFirstSubmit) &&
            const DeepCollectionEquality().equals(other.appUser, appUser) &&
            const DeepCollectionEquality()
                .equals(other.hasSentResetLink, hasSentResetLink));
  }

  @override
  int get hashCode => Object.hash(
      runtimeType,
      const DeepCollectionEquality().hash(isLoadingLogin),
      const DeepCollectionEquality().hash(isLoadingCreateAccount),
      const DeepCollectionEquality().hash(isLoadingResetPassword),
      const DeepCollectionEquality().hash(failureLogin),
      const DeepCollectionEquality().hash(failureResetPassword),
      const DeepCollectionEquality().hash(failureCreateAccount),
      const DeepCollectionEquality().hash(email),
      const DeepCollectionEquality().hash(password),
      const DeepCollectionEquality().hash(emailIsValid),
      const DeepCollectionEquality().hash(passwordIsValid),
      const DeepCollectionEquality().hash(hasAttemptedFirstSubmit),
      const DeepCollectionEquality().hash(appUser),
      const DeepCollectionEquality().hash(hasSentResetLink));

  @JsonKey(ignore: true)
  @override
  _$AuthStateCopyWith<_AuthState> get copyWith =>
      __$AuthStateCopyWithImpl<_AuthState>(this, _$identity);
}

abstract class _AuthState implements AuthState {
  const factory _AuthState(
      {final bool? isLoadingLogin,
      final bool? isLoadingCreateAccount,
      final bool? isLoadingResetPassword,
      final String? failureLogin,
      final String? failureResetPassword,
      final String? failureCreateAccount,
      final String? email,
      final String? password,
      final bool? emailIsValid,
      final bool? passwordIsValid,
      final bool? hasAttemptedFirstSubmit,
      final AppUser? appUser,
      final bool? hasSentResetLink}) = _$_AuthState;

  @override //
  bool? get isLoadingLogin => throw _privateConstructorUsedError;
  @override
  bool? get isLoadingCreateAccount => throw _privateConstructorUsedError;
  @override
  bool? get isLoadingResetPassword => throw _privateConstructorUsedError;
  @override //
  String? get failureLogin => throw _privateConstructorUsedError;
  @override
  String? get failureResetPassword => throw _privateConstructorUsedError;
  @override
  String? get failureCreateAccount => throw _privateConstructorUsedError;
  @override //
  String? get email => throw _privateConstructorUsedError;
  @override
  String? get password => throw _privateConstructorUsedError;
  @override //
  bool? get emailIsValid => throw _privateConstructorUsedError;
  @override
  bool? get passwordIsValid => throw _privateConstructorUsedError;
  @override //
  bool? get hasAttemptedFirstSubmit => throw _privateConstructorUsedError;
  @override //
  AppUser? get appUser => throw _privateConstructorUsedError;
  @override //
  bool? get hasSentResetLink => throw _privateConstructorUsedError;
  @override
  @JsonKey(ignore: true)
  _$AuthStateCopyWith<_AuthState> get copyWith =>
      throw _privateConstructorUsedError;
}
