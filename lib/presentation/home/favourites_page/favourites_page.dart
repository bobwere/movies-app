import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:flutter_staggered_animations/flutter_staggered_animations.dart';
import 'package:movies/models/movie/movie.dart';
import 'package:movies/presentation/home/details_page/details_page.dart';
import 'package:movies/presentation/widget/animated_card.dart';
import 'package:movies/services/movies/movies_provider/movies_provider.dart';
import 'package:movies/services/movies/movies_state/movies_state.dart';

class FavouritesPage extends ConsumerStatefulWidget {
  FavouritesPage({Key? key}) : super(key: key);

  @override
  _FavouritesPageState createState() => _FavouritesPageState();
}

class _FavouritesPageState extends ConsumerState<FavouritesPage> {
  ScrollController scrollController = ScrollController();

  @override
  Widget build(BuildContext context) {
    return getWidgetToDisplay(ref.watch(moviesProvider).value!, context);
  }

  Widget getWidgetToDisplay(MoviesState state, BuildContext context) {
    if (state.isLoadingFavouritesMovies!) {
      //loding state
      return buildLoadingWidget();
    }
    if (state.failureGettingFavouritesMovies!.isNotEmpty) {
      //error widget
      return buildErrorWidget(context, state.failureGettingMovies!);
    }
    if (state.favouriteMovies!.isEmpty) {
      //no movie results found kindly search again
      return buildNoResultsWidget(context);
    }
    if (state.favouriteMovies!.isNotEmpty) {
      //show results
      return buildLoadedWidget(state.favouriteMovies!);
    }
    return Container();
  }

  Widget buildLoadingWidget() {
    return Container(
      child: Center(
        child: SizedBox(
            height: 16,
            width: 15,
            child: CircularProgressIndicator(
              color: Colors.white,
            )),
      ),
    );
  }

  Widget buildErrorWidget(BuildContext context, String message) {
    return Container(
      child: Center(
          child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        crossAxisAlignment: CrossAxisAlignment.center,
        children: [
          Container(
            width: MediaQuery.of(context).size.width - 40,
            child: Center(
              child: Text(
                message,
                textAlign: TextAlign.center,
                style: TextStyle(
                    fontFamily: 'Montserrat',
                    fontWeight: FontWeight.w600,
                    fontSize: 16,
                    color: Colors.black87),
              ),
            ),
          ),
        ],
      )),
    );
  }

  Widget buildNoResultsWidget(BuildContext context) {
    return Container(
      child: Center(
          child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        crossAxisAlignment: CrossAxisAlignment.center,
        children: [
          Container(
            width: MediaQuery.of(context).size.width - 40,
            child: Center(
              child: Text(
                'No movies in your favourites',
                textAlign: TextAlign.center,
                style: TextStyle(
                    fontFamily: 'Montserrat',
                    fontWeight: FontWeight.w600,
                    fontSize: 16,
                    color: Colors.black87),
              ),
            ),
          ),
        ],
      )),
    );
  }

  Widget buildLoadedWidget(List<Movie> movies) {
    return AnimationLimiter(
      child: CustomScrollView(
        controller: scrollController,
        slivers: [
          SliverToBoxAdapter(
            child: Container(
              height: 140,
              color: Colors.blueGrey,
              child: CachedNetworkImage(
                imageUrl:
                    'https://images-wixmp-ed30a86b8c4ca887773594c2.wixmp.com/f/72c90880-a922-4b5d-aa67-6a058d88f48b/d3idj3c-c0e3ab91-32d1-4e57-859b-051c872ea0f6.jpg?token=eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJzdWIiOiJ1cm46YXBwOjdlMGQxODg5ODIyNjQzNzNhNWYwZDQxNWVhMGQyNmUwIiwiaXNzIjoidXJuOmFwcDo3ZTBkMTg4OTgyMjY0MzczYTVmMGQ0MTVlYTBkMjZlMCIsIm9iaiI6W1t7InBhdGgiOiJcL2ZcLzcyYzkwODgwLWE5MjItNGI1ZC1hYTY3LTZhMDU4ZDg4ZjQ4YlwvZDNpZGozYy1jMGUzYWI5MS0zMmQxLTRlNTctODU5Yi0wNTFjODcyZWEwZjYuanBnIn1dXSwiYXVkIjpbInVybjpzZXJ2aWNlOmZpbGUuZG93bmxvYWQiXX0.4mRTDDj8mHfSCbeXI_CTT4jD_IxfJfIXJUS-ULp0-70',
                fit: BoxFit.cover,
              ),
            ),
          ),
          SliverAppBar(
            automaticallyImplyLeading: false,
            centerTitle: true,
            title: Text(
              'Harry Potter Movies',
              style: TextStyle(
                  fontFamily: 'Montserrat',
                  fontSize: 20,
                  color: Colors.black,
                  fontWeight: FontWeight.bold),
            ),
            pinned: true,
            backgroundColor: Colors.transparent,
            elevation: 0,
          ),
          SliverToBoxAdapter(
            child: Container(
              margin: EdgeInsets.symmetric(vertical: 10),
              height: 10,
            ),
          ),
          SliverList(
            delegate: SliverChildBuilderDelegate((context, index) {
              final movie = movies[index];

              return AnimationConfiguration.staggeredList(
                position: index,
                duration: const Duration(milliseconds: 375),
                child: SlideAnimation(
                  horizontalOffset: 50.0,
                  child: FadeInAnimation(
                    child: GestureDetector(
                      onTap: () {
                        Navigator.push(
                          context,
                          MaterialPageRoute(
                            builder: (context) => DetailsPage(
                              movie: movie,
                            ),
                          ),
                        );
                      },
                      child: AnimatedCard(
                        controller: scrollController,
                        movie: movie,
                        imageUrl: movie.poster ?? '',
                        index: index,
                      ),
                    ),
                  ),
                ),
              );
            }, childCount: movies.length),
          )
        ],
      ),
    );
  }
}
